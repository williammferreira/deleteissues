const axios = require("axios")

var isDeleting = false
var projectID = "XXXXXXX"

axios
  .get(
    "https://gitlab.com/api/v4/projects/<PROJECT_IDs>/issues",
    {
      headers: {
        "PRIVATE-TOKEN": "<PRIVATE_TOKEN>",
      },
    }
  )
  .then(function (issues) {
    console.log(issues)
    for (const x of issues.data) {
      deleteIssue(x.iid)
    }

    if (issues.length == 0) {
      console.log("ALL ISSUES DELETED !!!")
    }
  })

function deleteIssue(id) {
  axios
    .delete("https://gitlab.com/api/v4/projects/<PROJECT_ID>/issues/" + id, {
      headers: {
        "PRIVATE-TOKEN": "<PRIVATE_TOKEN>",
      },
    })
    .then(function () {
      console.log(id + " DELETED")
    })
}